from flask import request, abort, jsonify

from flask import Blueprint

dispatcher = Blueprint('dispatcher', __name__)


# # @todo Specific exceptions.
# @dispatcher.errorhandler(ApiException)
# def api_exception_handler(error):
#     return str(error), 500
#
#
# @todo Specific exceptions.
@dispatcher.errorhandler(Exception)
def git_exception_handler(error):
    return str(error), 500


@dispatcher.route('/', methods=['GET'])
def index():
    result = ""
    return jsonify({"response": result})


@dispatcher.route('/register/<domain>/<name>/<repo>', methods=['GET'])
def register(domain=None, name=None, repo=None):
    from gitlog.api import LocalApi
    branch = request.args.get('branch', 'master')
    LocalApi(domain, name, repo, branch).register()
    # return repo hash
    return jsonify({"response": "ok"})


@dispatcher.route('/commits/<domain>/<name>/<repo>', methods=['GET'])
def commits(domain=None, name=None, repo=None):
    from gitlog.api import LocalApi
    branch = request.args.get('branch', 'master')
    local_api = LocalApi(domain, name, repo, branch)
    if local_api.git_cli_model.validate_remote():
        print("VALID")
        response = LocalApi(domain, name, repo, branch).get_commits(branch)
    else:
        print("NOT VALID")
        raise Exception("Repository has not been registered yet.")

    print("RESPONSE")
    print(response)
    # return repo hash
    return jsonify({"response": response})

#
#
# @flask_app.route('/commits/<hash>/<path:branch>', methods=['GET'])
# def commits(hash=None, branch='master'):
#     return jsonify({"response": "ok"})
