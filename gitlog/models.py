from gitlog.bootstrap import db

BRANCH_STATUS_NEW = 0
BRANCH_STATUS_CLONED = 1


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(255), unique=True, nullable=False)
    repos = db.relationship('Repo', backref='user', lazy=True)


class Repo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String(255), unique=True, nullable=False)
    url_hash = db.Column(db.String(255), unique=True, nullable=False)
    domain = db.Column(db.String(255), nullable=False)
    name = db.Column(db.String(255), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    branches = db.relationship('Branch', backref='repo', lazy=True)


class Branch(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    status = db.Column(db.Integer, nullable=False, default=BRANCH_STATUS_NEW)
    last_commit_id = db.Column(db.String(255), nullable=True)
    last_commit_hash = db.Column(db.String(255), nullable=True)
    last_commit_date = db.Column(db.String(255), nullable=True)
    repo_id = db.Column(db.Integer, db.ForeignKey('repo.id'), nullable=False)
    commits = db.relationship('Commit', backref='branch', lazy=True)


# It's arguable whether indexing commits is necessary but could be beneficial for sorting in this case.
# Other indexes such as "name" could also be useful.
class Commit(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    commit_hash = db.Column(db.String(255), nullable=False)
    author_date = db.Column(db.DateTime, nullable=False)
    committer_date = db.Column(db.DateTime, nullable=False)

    branch_id = db.Column(db.Integer, db.ForeignKey('branch.id'), nullable=False)

