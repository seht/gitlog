from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from tinydb import TinyDB

from gitlog.settings import CELERY_BROKER_URL, CELERY_RESULT_BACKEND
from gitlog.controllers import dispatcher

flask_app = Flask(__name__)

flask_app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///./index.db'
flask_app.config['CELERY_BROKER_URL'] = CELERY_BROKER_URL
flask_app.config['CELERY_RESULT_BACKEND'] = CELERY_RESULT_BACKEND

db = SQLAlchemy(flask_app)
db.init_app(flask_app)
db.create_all()

storage = TinyDB('storage.json')

flask_app.register_blueprint(dispatcher, url_prefix='/')
