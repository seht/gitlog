from celery import Celery
import shlex
import subprocess
from gitlog.bootstrap import flask_app
from gitlog.settings import CELERY_RESULT_BACKEND, CELERY_BROKER_URL


celery_app = Celery(flask_app.import_name, backend=CELERY_RESULT_BACKEND, broker=CELERY_BROKER_URL, include=[])
celery_app.conf.update(flask_app.config)


# @todo
# class RunCommand(celery.Task):
#
#     name = 'RunCommand'
#
#     def run(self, command, *args, **kwargs):
#         pass


def run_command(command, **kwargs):
    cmd = shlex.split(command)
    return subprocess.Popen(cmd, **kwargs)


@celery_app.task(name='run_command')
def run_command(command):
    process = run_command(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stderr = process.stderr.read()
    stdout = process.stdout.read()
    if stderr:
        print(stderr)
        # raise GitException(str(stderr))
        return str(stderr)
    return str(stdout)
