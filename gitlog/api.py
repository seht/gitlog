import hashlib
import os.path
import subprocess
import threading
import re

from gitlog.settings import REPO_DIR


def get_repo_url(domain, name, repo):
    # @todo Validate.
    # @todo Support https protocol.
    return 'git@{}:{}/{}.git'.format(domain, name, repo)


def get_commands(commands):
    return ";".join(commands)


# @todo Log.
def get_git_commands(git_dir, commands):
    return get_commands(['git --git-dir {} {}'.format(git_dir, cmd) for cmd in commands])


# @todo To handle return codes/errors, queueing could be implemented.
# @see gitlog.tasks
def run_commands(cmd):
    print("RUN COMMAND")
    print(cmd)
    process = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    return process


def run_thread_commands(cmd, callback=None, args=None):
    def run_process():
        print("THREAD CALLBACK ARGS")
        print(args)
        process = run_commands(cmd)
        process.wait()
        if callback:
            if args:
                return callback(*args)
            return callback()

    thread = threading.Thread(target=run_process)
    thread.start()
    return thread


# class GitHubApi(object):
#     def __int__(self, url):
#         pass
# "https://developer.github.com/v3/repos/commits/#list-commits-on-a-repository"
# "/repos/:owner/:repo/commits"?page=1&per_page=1
#


class GitCliModel(object):
    def __init__(self, repo_dir=None, remote=None, branch='master'):
        self.git_dir = self._get_git_dir(repo_dir)
        print("MODEL BRANCH")
        print(branch)
        self.remote = remote
        self.branch = branch

    @staticmethod
    def _get_git_dir(repo_dir):
        # git_dir = os.path.join(repo_dir, '.git')
        git_dir = repo_dir
        return git_dir

    def run_git_commands(self, commands, callback=None, args=None, wait=False):
        cmd = get_git_commands(self.git_dir, commands)
        if wait:
            process = run_commands(cmd)
            output, error = process.communicate()
            return output.decode("utf-8").strip()
        return run_thread_commands(cmd, callback=callback, args=args)

    def set_branch(self):
        self.branch = self.run_git_commands(["rev-parse --abbrev-ref HEAD"], wait=True)
        print("SET BRANCH")
        print(self.branch)
        return self.branch

    def switch_branch(self, branch=None, force=False, wait=False):
        if not branch:
            branch = self.branch
        if not force and branch == self.branch:
            return True
        commands = [
            "fetch origin {}:{}".format(branch, branch),
            "symbolic-ref HEAD refs/heads/{}".format(branch),
            # "update-ref"
        ]
        if branch == self.branch:
            self.run_git_commands(commands, wait=wait)
        else:
            self.run_git_commands(commands, self.set_branch, wait=wait)

    def validate_repo(self):
        return self.validate_remote()

    def pull(self):
        self.run_git_commands('pull origin {}'.format(self.branch))

    def repo_changed(self):
        # @todo
        # "git fetch origin"
        # "git remote update"
        # "git diff origin/master"
        # "git diff origin/master --shortstat"
        # "git status"
        pass

    def validate_remote(self):
        remote_origin = self.run_git_commands(['config --get remote.origin.url'], wait=True)
        if remote_origin == self.remote:
            return True
        return False

    # 'git --git-dir <hashed-repo/.git> --no-pager log --pretty=format:"%h%x09%an%x09%ad%x09%s" <last-commit-hash>'
    def get_commits(self, pretty=None, last_commit=None):
        log_format = '--pretty='
        log_format += pretty if pretty else 'oneline'

        cmd = '--no-pager log ' + log_format
        if last_commit:
            cmd += last_commit
        output = self.run_git_commands([cmd], wait=True)
        return output


class LocalApi(object):
    def __init__(self, domain=None, name=None, repo=None, branch='master'):
        self.dir_name = self._get_dir_name(domain, name, repo)
        self.repo_dir = self._get_repo_dir(self.dir_name)
        self.git_cli_model = GitCliModel(self.repo_dir, get_repo_url(domain, name, repo), branch)

    def register(self):
        commands = [
            'mkdir -p {}'.format(self.repo_dir),
            'git clone --bare --no-checkout --single-branch -b {} {} {}'.format(self.git_cli_model.branch,
                                                                                self.git_cli_model.remote,
                                                                                self.repo_dir),
        ]
        # Cloning a single branch would not require switching branch.
        # @todo Create models.Branch instance on successful clone and update status and last_commit.
        # @todo Populate (storage.json) with the commits.
        run_thread_commands(get_commands(commands), callback=self.git_cli_model.switch_branch,
                            args=[self.git_cli_model.branch])
        # #@todo Validate remote.

    @staticmethod
    def _get_dir_name(domain, name, repo):
        h = hashlib.new('sha1')
        path = os.path.join(domain, name, repo).encode('utf-8')
        h.update(path)
        return h.hexdigest()

    @staticmethod
    def _get_repo_dir(dir_name):
        git_dir = os.path.join(REPO_DIR, dir_name)
        return git_dir

    # API
    # @todo Check for models.Branch instance cloned status and get commits from storage {storage.json} indexed by {gitlog.models}
    def get_commits(self, branch=None, per_page=1, page=1):
        # https://git-scm.com/docs/pretty-formats
        author_name = '%aN'  # .mailmap
        # committer_name = '%cN'
        # author_email = '%aE'
        # author_date = '%aD'  # RFC2822
        author_date = '%aI'  # ISO8601
        commit_hash = '%H'
        message = '%s'
        separator = '%x09'

        line = [
            commit_hash,
            author_name,
            author_date,
            message,
        ]

        pretty_format = 'format:' + separator.join(line)

        if branch:
            self.git_cli_model.switch_branch(branch=branch, force=True, wait=True)

        commits = self.git_cli_model.get_commits(pretty=pretty_format)

        lines = commits.splitlines()

        response = []

        for row in lines:
            cols = re.split(r'\t+', row)
            response.append({
                'hash': cols[0],
                'author': cols[1],
                'commit_date': cols[2],
                'message': cols[3],
            })

        return response
