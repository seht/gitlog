# gitlog

# Quick start
```
virtualenv -p /usr/bin/python3 env
source env/bin/activate
pip install -r requirements.txt
```

```
export FLASK_APP=run.py
flask run
```

(Not enabled: $ celery -A gitlog.tasks.celery_app worker)

- This is the fallback implementation using only Git CLI.

- Works with GitLab and GitHub on public repos.
- Persistence is to be enabled relationally and non-relationally (index.db/storage.json).

- There's a basic queueing implementation using Redis, although this is super seeded
by threading in this example.

- There are enough placeholders of where Exceptions should be raised but even if it is mostly
graceful there are considerable usage case limitations.


## Usage

### API

Only the endpoints bellow have been described and serve as example usage.


```
curl -v http://127.0.0.1:5000/register/gitlab.com/seht/gitlog-test?branch=test/test-branch

curl -v http://127.0.0.1:5000/commits/gitlab.com/seht/gitlog-test?branch=test/test-branch
```
